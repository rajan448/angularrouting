import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users = [
    {
      id: 1,
      name: 'Rajan'
    },
    {
      id: 2,
      name: 'Manish'
    },
    {
      id: 3,
      name: 'Jyoti'
    }
  ]
  constructor() { }

  ngOnInit() {
  }

}
