import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ServerService {

  servers = [
    {id: 1, name: 'Production Server', status: 'online'},
    {id: 2, name: 'Dev Server', status: 'offline'},
    {id: 3, name: 'UAT Server', status: 'online'}
  ];

  constructor() {
  }

  getServers() {
    return this.servers;
  }

  public getServer(id: number) {
    return this.servers.find(
      server => server.id === id
    );
  }

  public updateServer(id: number, serverStatus: { name: string, status: string }) {
    const server = this.servers.find(
      s => s.id === id
    );
    if (server) {
      server.name = serverStatus.name;
      server.status = serverStatus.status;
    }
  }

}
