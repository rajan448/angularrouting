import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  loggedIn = false;

  constructor() {
  }

  public login() {
    this.loggedIn = true;
    console.log('Logged in');
  }

  public logout() {
    this.loggedIn = false;
    console.log('Logged Out');
  }

  public isAuthenticated() {
    const promise = new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          resolve(this.loggedIn);
        }, 200);
      }
    );
    return promise;
  }


}
