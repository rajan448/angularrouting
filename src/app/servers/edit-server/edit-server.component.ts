import {Component, OnInit} from '@angular/core';
import {ServerService} from '../../services/server.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {CanComponentDeactivate} from './can-deactivate.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-edit-server',
  templateUrl: './edit-server.component.html',
  styleUrls: ['./edit-server.component.scss']
})
export class EditServerComponent implements OnInit, CanComponentDeactivate {

  server: { id: number, name: string, status: string };
  public serverName: string;
  public serverStatus: string;
  public allowEdit = false;
  public changesSaved = false;

  constructor(
    private serverService: ServerService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    /*
        console.log(this.route.snapshot.queryParams);
        console.log(this.route.snapshot.fragment);
    */
    this.route.queryParams.subscribe((params: Params) => this.allowEdit = params['allowEdit'] === '1');
    // this.route.fragment.subscribe();

    this.route.params.subscribe((params: Params) => {
        this.server = this.serverService.getServer(+params['id']);
        this.serverName = this.server.name;
        this.serverStatus = this.server.status;
      }
    );
  }

  onUpdateServer() {
    this.serverService.updateServer(
      this.server.id, {name: this.serverName, status: this.serverStatus});
    this.changesSaved = true;
    this.router.navigate(['../'], {relativeTo: this.route});
  }

  canDeactivate(): Observable<boolean> | Promise<boolean> | boolean {
    if (!this.allowEdit) {
      return true;
    }
    if ((this.serverStatus !== this.server.status || this.serverName !== this.server.name) && this.changesSaved === false) {
      return confirm('Do you want to discard the change');
    } else {
      return true;
    }
  }
}
