import {Component, OnInit} from '@angular/core';
import {ServerService} from '../../services/server.service';
import {ActivatedRoute, Data, Router} from '@angular/router';

@Component({
  selector: 'app-server',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.scss']
})
export class ServerComponent implements OnInit {

  server: { id: number, name: string, status: string };
  id: number;

  constructor(
    private serverService: ServerService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
/*
    this.route.params.subscribe(
      params => {
        this.id = +params['id'];
        this.server = this.serverService.getServer(this.id);
      });
  */

  this.route.data.subscribe(
    (data: Data) => {
      this.server = data['server'];
    }
  );

  }

  onEdit() {
    this.router.navigate(['edit'], {relativeTo: this.route, queryParamsHandling: 'preserve'});
  }
}
