import {Component, OnInit} from '@angular/core';
import {ServerService} from '../services/server.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.scss']
})
export class ServersComponent implements OnInit {

  private servers: { id: number, name: string, status: string }[] = [];

  constructor(
    private serverService: ServerService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.servers = this.serverService.getServers();
  }

  onReload() {
    // Path is always relative with or without /

    // '/' only have meaning when used with relativeTo property
    /*
        this.router.navigate(['servers'], {
            relativeTo: this.route
          }
        );
     */
  }
}
